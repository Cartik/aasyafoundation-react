import firebase from "firebase/app";
import "firebase/storage";

var config = {
  apiKey: "AIzaSyCEVObtKc8BLPY8z0YTwWlYtFpt2PPO9Ys",
  authDomain: "aasyafoundation-b1427.firebaseapp.com",
  databaseURL: "https://aasyafoundation-b1427.firebaseio.com",
  projectId: "aasyafoundation-b1427",
  storageBucket: "aasyafoundation-b1427.appspot.com",
  messagingSenderId: "979352197002",
  appId: "1:979352197002:web:636a70e573b32f09f65a73",
  measurementId: "G-8G10PMZJW7",
};

firebase.initializeApp(config);

var storage = firebase.storage();

export { storage, firebase as default };
