import React from "react";

import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Link from '@material-ui/core/Link';
import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';
import TwitterIcon from '@material-ui/icons/Twitter';
import Card from '@material-ui/core/Card';
import Container from '@material-ui/core/Container';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	footerFlex: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	footer: {
		borderTop: '0.2rem solid #E0E0E0',
		margin: '1.8rem 0 0 0',
		padding: '1rem 0'
	},
	footerSubtitle: {
		fontSize: '1.2rem',
		textTransform: 'uppercase',
		textAlign: 'center',
		letterSpacing: '5px'
	},
	footerSocialWrap: {
		textAlign: 'center',
		padding: '2rem 3rem',
    	width: '320px',
    	margin: '2.5rem auto',
		borderRadius: '1rem',
		boxShadow: '0 0 5px 2px rgba(0,0,0,0.05)'
	},
	footerCopy: {
		margin: '1.5rem 0',
		padding: '1.2rem 0 0 0',
		borderTop: '0.1rem solid #E0E0E0',
		fontSize: '1.2rem',
    	letterSpacing: '1px',
		fontWeight: 'normal',
		textAlign: 'center'
	}
}));

export default function Footer() {

	const classes = useStyles();

	return (
		<footer className={classes.footer}>
			<Container fixed>
				<Toolbar className={classes.footerFlex}>
					<div>
						<Typography component="h5" variant="h5">Aasya Foundation</Typography>
						<Typography component="p" variant="subtitle1" className={classes.footerSubtitle}>Cure is in care</Typography>
					</div>
					<MenuList className={classes.footerFlex}>
						<MenuItem>
							<Link href="/" color="textSecondary">Terms &amp; Conditions</Link>
						</MenuItem>
						<MenuItem>
							<Link href="/about" color="textSecondary">Refund &amp; Cancellation Policy</Link>
						</MenuItem>
						<MenuItem>
							<Link href="/events" color="textSecondary">Privacy Policy</Link>
						</MenuItem>
						<MenuItem>
							<Link href="/contact" color="textSecondary">Internships</Link>
						</MenuItem>
					</MenuList>
				</Toolbar>
				<Card className={classes.footerSocialWrap}>
					<Typography component="h5" variant="h5">We are Social</Typography>
					<br/>
					<div className={classes.footerFlex}>
						<Link href="https://www.instagram.com/Aasyafoundation" target="_blank" rel="noopener">
							<InstagramIcon style={{ fontSize: 30 }} color="action"/>
						</Link>
						<Link href="https://www.facebook.com/Aasyafoundation" target="_blank" rel="noopener">
							<FacebookIcon style={{ fontSize: 30 }} color="action"/>
						</Link>
						<Link href="https://www.youtube.com/channel/UCT-L00Au3qEYMngNLI0NyLw" target="_blank" rel="noopener">
							<YouTubeIcon style={{ fontSize: 30 }} color="action"/>
						</Link>
						<Link href="https://www.twitter.com/Aasyafoundation" target="_blank" rel="noopener">
							<TwitterIcon style={{ fontSize: 30 }} color="action"/>
						</Link>
					</div>
				</Card>
				<Typography component="p" variant="subtitle1" className={classes.footerCopy}>&copy; Copyrights 2020 Aasya Foundation All Rights Reserved.</Typography>
			</Container>
		</footer>
	);

}