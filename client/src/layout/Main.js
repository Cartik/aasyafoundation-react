import React, { Fragment } from "react";

import Header from "./Header";
import Footer from "./Footer";

class Main extends React.Component {
	render() {
		const { classes, children } = this.props;

		return (
			<Fragment>
				<Header />
				{children}
				<Footer />
			</Fragment>
		);
	}
}
export default Main;
