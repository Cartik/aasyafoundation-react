import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Link from '@material-ui/core/Link';
import Container from '@material-ui/core/Container';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
	headerMain: {
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	headerBoxShadow: {
		boxShadow: 'inset 0 30px 15px 2px rgba(0,0,0,0.2)',
		padding: '1.2rem 0'
	}
}));

export default function Header() {
	const classes = useStyles();

	return (		
		<AppBar color="transparent" className={classes.headerBoxShadow}>
			<Container>
				<Toolbar className={classes.headerMain}>
					<Link href="#/">
						<img src="http://aasyafoundation.org/images/logo.png" title="Aasya Foundation | Cure is in Care" alt="Aasya Foundation" width="90"/>
					</Link>
					<MenuList className={classes.headerMain}>
						<MenuItem>
							<Link href="#/" color="textPrimary">Home</Link>
						</MenuItem>
						<MenuItem>
							<Link href="#/about" color="textPrimary">About Us</Link>
						</MenuItem>
						<MenuItem>
							<Link href="#/events" color="textPrimary">Events</Link>
						</MenuItem>
						<MenuItem>
							<Link href="#/contact" color="textPrimary">Contact Us</Link>
						</MenuItem>
						<MenuItem>
							<Link href="#/register" color="textPrimary">Join Us</Link>
						</MenuItem>
					</MenuList>
				</Toolbar>
			</Container>
		</AppBar>
	)
}