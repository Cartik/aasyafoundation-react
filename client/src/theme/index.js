import { createMuiTheme } from '@material-ui/core';
import palette from './palette';

export const theme = createMuiTheme({
  palette,
  typography: {
    
    htmlFontSize: 10,

    h1:{
      fontFamily: 'Merriweather, serif',
      fontWeight: 'bold',
      fontSize: '4.5rem',
      margin: '0 0 15px 0'
    },
    h2:{
      fontFamily: 'Merriweather, serif',
      fontWeight: 'bold',
      fontSize: '3.2rem',
      margin: '0 0 15px 0'
    },
    h3:{
      fontFamily: 'Merriweather, serif',
      fontWeight : 700,
      fontSize: '2.8rem',
      margin: '0 0 15px 0'
    },
    h4:{
      fontFamily: 'Merriweather, serif',
      fontWeight : 700,
      fontSize: '2.4rem',
      margin: '0 0 12px 0'
    },
    h5:{
      fontFamily: 'Merriweather, serif',
      fontWeight : 700,
      color: '#454545',
      fontSize: '1.8rem',
      margin: '0 0 12px 0'
    },
    h6:{
      fontFamily: 'Lato, sans-serif',
      fontWeight : 400,
      fontSize: '1.4rem',
      color: '#bbb',
      textTransform: 'uppercase'
    },
    body1:{
      fontWeight: 400,
      lineHeight: 1.5,
      fontSize: '1.5rem',
      fontFamily: 'Lato, sans-serif',
      color: '#555555',
    },
    body2:{
      fontWeight: 400,
      lineHeight: 1.5,
      fontSize: '1.5rem',
      fontFamily: 'Lato, sans-serif',
      color: '#555555'
    },
    subtitle1:{
      fontWeight: 700,
      fontSize: '1.5rem',
      fontFamily: 'Lato, sans-serif',
      color: '#555555',
      lineHeight: 'normal'
    }
  }
})