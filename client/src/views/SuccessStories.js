import { Avatar, Card, CardActionArea, CardContent, Chip, Container, Grid, Paper, Typography } from "@material-ui/core";
import grey from "@material-ui/core/colors/grey";
import { withStyles } from "@material-ui/styles";
import React, { Fragment } from "react";
import Dotdotdot from 'react-dotdotdot';
import ReactLoading from 'react-loading';
import Slide from "react-reveal/Slide";
import AasyaApi from "../api/AasyaApi";
import ParallexMain from "../components/ParallexMain";
import StoryCard from "../components/StoryCard";
import TextInImage from "../components/TextInImage";

const styles = (theme) => ({
  root: {
    height: "100%",
    backgroundColor: grey[100],
  },
  successStoriesContainer: {
    paddingTop: theme.spacing(6),
  },
  carouselContainer: {
    position: "relative",
    display: "flex",
    alignItems: "middle",
    height: "100%",
    width: "100%",
  },
  margin: {
    marginLeft: "auto",
    marginRight: "auto",
  },
  cardContent: {
    padding:0,
    paddingLeft:16
  },
  avatar: {
    width: 60,
    height: 60,
  },
  actionArea: {
    display: "flex",
    justifyContent: "start",
    padding: theme.spacing(1),
  },
  card: {
    marginBottom: theme.spacing(1),
    opacity:0.5
  },
  paperContainer: {
    backgroundColor: grey[200],
  },
  storiesChip: {
    marginTop: -28,
    color: "white",
    marginLeft: 8,
  },
  otherStoriesConatiner: {
    padding: theme.spacing(2),
    overflow: "scroll",
    maxHeight: 500,
  },
});
class SuccessStories extends React.Component {
  state = {
    stories:[],
    mainStory:{},
    loaded:false,
  };
  componentDidMount(){
    AasyaApi.get('/sucessstories').then(responce => {
      this.setState({mainStory:responce.data.shift(),stories:responce.data,loaded:true})
    }).catch( error =>{
      console.log("Errors") //TODO catch need to find
    })
  }
  
  handleCardClick = (storyid) =>{
    this.setState({mainStory : this.state.stories.find(story => {
        return story._id === storyid
    })})
  }
  render() {
    const { match,classes } = this.props;
    console.log(this.props)
    console.log(match)
    console.log(process.env)
    return (
      <Fragment>
        {this.state.loaded ? <div className={classes.root}>
        <ParallexMain Image="https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg">
          <TextInImage variant="h1" text="Our Success Stories "/>
        </ParallexMain>
        <Container maxWidth="xl">
          <Grid
            container
            spacing={4}
            className={classes.successStoriesContainer}
          >
            <Grid item xs={12} sm={12} md={8}>
              <Grid container>
                <Grid item xs={12} sm={12} md={10} className={classes.margin}>
                  <StoryCard mainStory = {this.state.mainStory}></StoryCard>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12} md={4}>
              <Paper className={classes.paperContainer}>
                <Chip
                  color="primary"
                  className={classes.storiesChip}
                  label="Other Stories"
                />
                <Slide right cascade>
                  <div className={classes.otherStoriesConatiner}>
                    {this.state.stories.map((story) => (
                      <Card key={story.id}className={classes.card}>
                        <CardActionArea className={classes.actionArea} onClick={() =>this.handleCardClick(story._id)}>
                          <Avatar
                            className={classes.avatar}
                            variant="rounded"
                            src={process.env.REACT_APP_STRAPI_URL+story.image.url}
                          />
                          <CardContent className={classes.cardContent}>
                            <Typography gutterBottom variant="subtitle1">
                              {story.name}
                            </Typography>
                            <Dotdotdot clamp={4}>
                            <Typography gutterBottom variant="body2">
                              {story.description}
                            </Typography>
                            </Dotdotdot>
                          </CardContent>
                        </CardActionArea>
                      </Card>
                    ))}
                  </div>
                </Slide>
              </Paper>
            </Grid>
          </Grid>
          {/* </Grid>
            <Grid item xs={12} sm={4} md={4}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={12} md={12}>
                  <Beneficiaries></Beneficiaries>
                </Grid>
                <Grid item xs={12} sm={12} md={12}>
                  <TwitterComponent></TwitterComponent>
                </Grid>
              </Grid>
            </Grid>
          </Grid> */}
        </Container>
      </div> : <ReactLoading type={"balls"} color={'black'}/>}
      </Fragment>
      
    );
  }
}

export default withStyles(styles)(SuccessStories);
