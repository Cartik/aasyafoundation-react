import React from 'react'
import ParallexMain from '../components/ParallexMain'
import TextInImage from '../components/TextInImage'

class HomePage extends React.Component{
    render(){
        return(
            <ParallexMain Image="http://aasyafoundation.org/images/slide-04.jpg">
                <TextInImage variant="h1" text="Aasya Foundation"></TextInImage>
            </ParallexMain>
        )
    }
}

export default HomePage