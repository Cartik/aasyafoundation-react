import React, { Fragment } from "react";
import { withStyles } from "@material-ui/styles";
import AboutUsMain from "../components/aboutus/AboutUsMain";
import TextInImage from "../components/TextInImage";

import Container from "@material-ui/core/Container";
import ParallexMain from "../components/ParallexMain";

const styles = (theme) => ({});

class AboutUs extends React.Component {
  render() {
    return (
      <Fragment>
        <ParallexMain Image="https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg">
          <TextInImage variant="h1" text="About Us"></TextInImage>
        </ParallexMain>
        <Container maxWidth="xl">
          <AboutUsMain />
        </Container>
      </Fragment>
    );
  }
}

export default withStyles(styles)(AboutUs);
