import React, { Fragment } from "react";
import {
  Container,
  GridList,
  GridListTile,
  GridListTileBar,
  Chip,
  Paper,
  Grid,
  Hidden,
  Box,
} from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import withWidth, { isWidthUp } from "@material-ui/core/withWidth";
import InfiniteScroll from "react-infinite-scroll-component";
import axios from "axios";
import grey from "@material-ui/core/colors/grey";
import TextInImage from "../components/TextInImage";
import ParallexMain from "../components/ParallexMain";
import ZigZagImages from "../components/ZigZagImages";
import YoutubeLinkComponent from "../components/YoutubeLinkComponent";
import TwitterComponent from "../components/TwitterComponent";
import Fade from 'react-reveal/Fade';

const styles = (theme) => ({
  mainContainer: {
    //backgroundColor: grey[100],
    paddingTop:theme.spacing(2)
  },
  gridList: {
    width: "100%",
    height: "100%",
    "& li": {},
    "& li > div": {
      borderRadius: 8,
    },
  },
  root: {
    height: "70vh",
    maxHeight: "70vh",
    width: "100%",
    marginTop: 16,
    overflow: "auto",
  },
  chipsConatiner: {
    padding: 16,
  },
  chip: {
    marginRight: theme.spacing(0.5),
  },
  youtubeLinkContainer: {
    height: '200px',
    position:'sticky',
    top:'30px'
  },
  youtubeItem: {
    height: '200px',
  },
});

const chipsData = [
  "Old Age Home ",
  "TimeWithTots",
  "Mahi",
  "Cancer",
  "Walkathon",
  "Anandhan",
];
class Gallery extends React.Component {
  state = {
    images: [],
    pageNumber: 1,
    ZigZagImages: [],
  };
  getGridListCols = () => {
    const { width } = this.props;
    if (isWidthUp("xl", width)) {
      return 4;
    }

    if (isWidthUp("lg", width)) {
      return 3;
    }

    if (isWidthUp("md", width)) {
      return 3;
    }

    return 1;
  };
  handleLoadMore = (page) => {
    console.log(page);
    console.log("Load More triggerd");
    //this.fetchImages();
  };
  componentDidMount() {
    let count = 14;
    const apiRoot = "https://pixabay.com/api";
    const accessKey = "9373449-95df096fc0b73a39b069f9716";
    const self = this;
    axios
      .get(
        `${apiRoot}/?key=${accessKey}&image_type="photo"&per_page=${count}&page=${self.state.pageNumber}`
      )
      .then(function (response) {
        self.setState({
          images: [...self.state.images, ...response.data.hits.slice(4)],
          ZigZagImages: [...response.data.hits.slice(0, 4)],
          pageNumber: self.state.pageNumber + 1,
        });
      });
  }
  fetchImages = (count = 14) => {
    const apiRoot = "https://pixabay.com/api";
    const accessKey = "9373449-95df096fc0b73a39b069f9716";
    const self = this;
    axios
      .get(
        `${apiRoot}/?key=${accessKey}&image_type="photo"&per_page=${count}&page=${self.state.pageNumber}`
      )
      .then(function (response) {
        self.setState({
          images: [...self.state.images, ...response.data.hits],
          pageNumber: self.state.pageNumber + 1,
        });
      });
  };
  render() {
    const { classes } = this.props;
    console.log(this.state.images);
    return (
      <Fragment>
        <ParallexMain Image="https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg">
          <TextInImage variant="h1" text="Our Gallery"></TextInImage>
        </ParallexMain>
        <ZigZagImages images={this.state.ZigZagImages}></ZigZagImages>
        <Container className={classes.mainContainer} maxWidth="xl">
          {/* <Paper className={classes.chipsConatiner}>
            {chipsData.map((chip) => (
              <Chip className={classes.chip} color="primary" label={chip} />
            ))}
          </Paper> */}
          <Grid container>
            <Grid item xs={12} sm={12} md={9}>
                <InfiniteScroll
                  dataLength={this.state.images.length}
                  next={() => this.fetchImages(5)}
                  hasMore={true}
                  loader={<div className="loader">Loading ...</div>}
                >
                  {
                   <Fade bottom cascade>
                    <GridList
                      cellHeight={250}
                      className={classes.gridList}
                      cols={this.getGridListCols()}
                      spacing={16}
                    >
                      {this.state.images.map((image, index) => (
                          <GridListTile key={image.id}>
                            <img src={image.largeImageURL} alt={image.user} />
                            <GridListTileBar
                              title={image.user}
                              titlePosition="bottom"
                            ></GridListTileBar>
                          </GridListTile>
                      ))}
                      
                    </GridList>
                   </Fade>
                  }
                </InfiniteScroll>
            </Grid>
            <Hidden smDown>
              <Grid className={classes.youtubeLinkContainer}item xs={12} sm={12} md={3}>
                <Box className={classes.youtubeItem} display="flex" justifyContent="center" alignItems="center">
                  <YoutubeLinkComponent></YoutubeLinkComponent>
                </Box>
                <TwitterComponent></TwitterComponent>
              </Grid>
            </Hidden>
          </Grid>
        </Container>
      </Fragment>
    );
  }
}

export default withStyles(styles)(withWidth()(Gallery));
