import React from 'react'
import {withStyles} from '@material-ui/styles'
import ContactUsMap from '../components/contactus/ContactUsMap';
import Feedback from '../components/contactus/Feedback';
import AasyaCities from '../components/contactus/AasyaCities';


const styles = theme => ({
    root:{
        height:'100%'
    },
    mapContainer:{
        height:'55vh',
        position:'relative',
        overflow:'hidden'
    },
    feedbackContainer:{
        margin:' -60px 30px 30px',
        boxShadow: '0 16px 24px 2px rgba(0, 0, 0, 0.14), 0 6px 30px 5px rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2)'
    }
})
class ContactUs extends React.Component{
    render(){
        const {classes} = this.props;
        return(
            <div className = {classes.root}>
                 <div className = {classes.mapContainer}>
                    <ContactUsMap></ContactUsMap>
                </div>
                <div className = {classes.feedbackContainer}>
                    <Feedback></Feedback>
                </div>
                <div>
                    <AasyaCities></AasyaCities>
                </div>
            </div>
        );
    }
}

export default withStyles(styles) (ContactUs)