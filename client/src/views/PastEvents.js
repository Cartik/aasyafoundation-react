import React from 'react'

import {
    Card,
    CardHeader,
    Grid,
    CardMedia,
    Typography,
    CardContent,
    CardActionArea,
    Chip,
    IconButton,
    CardActions,
    Paper,
    Divider,
    Box,
    Checkbox,
    FormControlLabel,
    
  } from "@material-ui/core";
import { withStyles } from '@material-ui/styles'
import LocationIcon from "@material-ui/icons/LocationOn";
import CallIcon from "@material-ui/icons/Call";
import MailIcon from "@material-ui/icons/Mail";
import CalenderIcon from "@material-ui/icons/DateRangeRounded";
const styles = theme => ({
    eventCard: {
        opacity: 0.9,
        borderRadius: theme.spacing(0.5),
        transition: "0.3s",
        display: "flex",
        flexDirection: "row",
        //alignItems: 'center',
        marginBottom: theme.spacing(2),
        //margin:'auto',
        overflow: "initial",
        boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
        "&:hover": {
          boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
        }
      },
      media: {
       
        marginLeft: "-16px",
        marginRight: "auto",
        marginTop: theme.spacing(2),
        boxShadow: "4px 8px 40px -8px rgba(0,0,0,1)",
        height: 0,
        borderRadius: theme.spacing(1),
        paddingBottom: "18%",
        paddingLeft : 150
      }
})
class PastEvents extends React.Component{
    
    render(){
        const {classes} = this.props;
        return(
            <Grid container>
                <Grid item xs='auto' sm ='auto' lg = 'auto'>
                <Card className={classes.eventCard}>
                <CardMedia
                  className={classes.media}
                  image="https://lh3.googleusercontent.com/-N5B-Q57bjcvZPfri_tbsasCCr5FPSIw0fbaED1Favy636I5A7_NExbTN_Z6OB92lm4CTPh2o1d6aZjwjagkN2GlotRo6FAchaHz7Ke5Fd3lwJYqlomibO-D_C8E6eL1iVnZ9yxWs3pe7ZiC0Vyw5xwCD-JLUPCFk4qmuoYv7y4k7jh5mlfSzdUxyRqNQrAT_l9OKoCAnrssIC4BN5oFyOGcqj0yZE2z0nyt7b1p1X2v0TMJ8BkiJiu92H5yXkP5TjZfqyzeIkeT7RGr-1dANfwzk0e1LcAeWMD9HzPJ5DpZz5tlX5RvJVFplc5laYLoivNrZHwz34WuosXQZsQWi9FL8zU70iXHmSAOj5uRGKzhq5xfSDR91vAX7SGLsCPrc2PUhvl94cPVOQCsPfOeOPPhKZs2pagyhTcKQWdc_Gxz-ZSU6QGa2et1MTZFO0_Gg_d1YDTSLbC7YpgO0Iv1Rw2Kwi8B1mQaQ9NXqVrJgPonJndIHtLt764XI9BmB8xMARlwsdZ7hveHC0TJPGF7xR4GsA3o2uFNLgYkHfpMU_0-lvai4o-GO9Sa_Hz4S9wzgwA1vC_-citwSnWZsP2PmjKLr5xTR1AOkZL0EjhDPDQxn9tb6uyqTD-uKDdwDSDjQvbeI84FMWIOWUfeQB3b-8pj9f4Mn3S_pVuhm-EI4eJnidp7-9t_53GOPCRl8bmWYrPzpRIvtr_S9TUScz73zDTB_ZNqvbEVyHEA48kim0s_yhw=w1902-h1262-no"
                ></CardMedia>
                <div>
                <CardContent>
                  <Typography
                    className={"MuiTypography--heading"}
                    variant="h6"
                    component="h2"
                  >
                    Food for orphons
                  </Typography>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                  >
                    It is used to make lot of events
                  </Typography>
                </CardContent>
                <Divider inset />
                <CardActions disableSpacing>
                  <Chip
                    label="22 sep 2012"
                    avatar={<CalenderIcon></CalenderIcon>}
                  ></Chip>
                  <IconButton>
                    <CallIcon />
                  </IconButton>
                  <IconButton>
                    <MailIcon />
                  </IconButton>
                  <IconButton>
                    <LocationIcon />
                  </IconButton>
                </CardActions>
                </div>
              </Card>
                </Grid>
            </Grid>
        )
    }
}

export default withStyles(styles) (PastEvents)