import React from "react";
import {
  Card,
  CardHeader,
  Grid,
  CardMedia,
  Typography,
  CardContent,
  CardActionArea,
  Chip,
  IconButton,
  CardActions,
  Paper,
  Divider,
  Box,
  Checkbox,
  FormControlLabel
} from "@material-ui/core";
import EventImage from "../images/aasya_event_1.jpg";
import LocationIcon from "@material-ui/icons/LocationOn";
import CallIcon from "@material-ui/icons/Call";
import MailIcon from "@material-ui/icons/Mail";
import CalenderIcon from "@material-ui/icons/DateRangeRounded";
import { withStyles } from "@material-ui/styles";
const styles = theme => ({
  eventsMainConatiner: {
    backgroundImage: `linear-gradient(rgba(0,0,0,0.7),rgba(0,0,0,0.7)),url(https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg)`,
    backgroundSize: "cover",
    padding: 15
  },
  eventsConatiner: {
    padding: 30
  },
  eventsChip: {
    color: "white"
  },
  root: {
    opacity: 0.9,
    borderRadius: theme.spacing(0.5),
    transition: "0.3s",
    display: "flex",
    //alignItems: 'center',
    marginBottom: theme.spacing(3),
    flexDirection: "column",
    //margin:'auto',
    overflow: "initial",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
    }
  },
  todayLabel:{
    color:'white'
  },
  dateContainer: {
    backgroundColor: "#9c27b0"
  },
  media: {
    width: "88%",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(-3),
    boxShadow: "4px 8px 40px -8px rgba(0,0,0,1)",
    height: 0,
    borderRadius: theme.spacing(1),
    paddingBottom: "48%"
  }
});

const events = [
  {
    eventId: "1",
    eventName: "FoodForOrphans",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "TimeWithTiny",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://lh3.googleusercontent.com/-N5B-Q57bjcvZPfri_tbsasCCr5FPSIw0fbaED1Favy636I5A7_NExbTN_Z6OB92lm4CTPh2o1d6aZjwjagkN2GlotRo6FAchaHz7Ke5Fd3lwJYqlomibO-D_C8E6eL1iVnZ9yxWs3pe7ZiC0Vyw5xwCD-JLUPCFk4qmuoYv7y4k7jh5mlfSzdUxyRqNQrAT_l9OKoCAnrssIC4BN5oFyOGcqj0yZE2z0nyt7b1p1X2v0TMJ8BkiJiu92H5yXkP5TjZfqyzeIkeT7RGr-1dANfwzk0e1LcAeWMD9HzPJ5DpZz5tlX5RvJVFplc5laYLoivNrZHwz34WuosXQZsQWi9FL8zU70iXHmSAOj5uRGKzhq5xfSDR91vAX7SGLsCPrc2PUhvl94cPVOQCsPfOeOPPhKZs2pagyhTcKQWdc_Gxz-ZSU6QGa2et1MTZFO0_Gg_d1YDTSLbC7YpgO0Iv1Rw2Kwi8B1mQaQ9NXqVrJgPonJndIHtLt764XI9BmB8xMARlwsdZ7hveHC0TJPGF7xR4GsA3o2uFNLgYkHfpMU_0-lvai4o-GO9Sa_Hz4S9wzgwA1vC_-citwSnWZsP2PmjKLr5xTR1AOkZL0EjhDPDQxn9tb6uyqTD-uKDdwDSDjQvbeI84FMWIOWUfeQB3b-8pj9f4Mn3S_pVuhm-EI4eJnidp7-9t_53GOPCRl8bmWYrPzpRIvtr_S9TUScz73zDTB_ZNqvbEVyHEA48kim0s_yhw=w1902-h1262-no",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "OldAgeHome Visit",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "FoodForOrphans",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "TimeWithTiny",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "Mission 500",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "Anandhan",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "Vouleenter Meet",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "Cancer Camp",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "Mahi",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "TimeWithTiny",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  },
  {
    eventId: "1",
    eventName: "OldAgeHome",
    DateOfEvent: "22-Sep-2020",
    phoneNumber: "9666616006",
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg",
    eventDescription: "It a wonder full event to map"
  }
];
class Events extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.eventsMainConatiner}>
        <Typography
          className={classes.eventsChip}
          weight={"bold"}
          variant={"h5"}
          bottomSpace={"large"}
        >
          Our Events
        </Typography>
        <FormControlLabel className ={classes.todayLabel}
        control={
          <Checkbox
            checked={true}
            value="checkedB"
            color="primary"
          />
        }
        label="Today Events"
      />
        <div className={classes.eventsConatiner}>
          <Grid container spacing={3}>
            {events.map(event => (
              <Grid item xs={12} sm={6} lg={3}>
                <Card className={classes.root}>
                  <CardMedia
                    className={classes.media}
                    image={event.imageUrl}
                  ></CardMedia>
                  <CardContent>
                    <Typography
                      className={"MuiTypography--heading"}
                      variant="h6"
                      component="h2"
                    >
                      {event.eventName}
                    </Typography>
                    <Typography
                      variant="body2"
                      color="textSecondary"
                      component="p"
                    >
                      {event.eventDescription}
                    </Typography>
                  </CardContent>
                  <Divider inset />
                  <CardActions disableSpacing>
                    <Chip
                      label={event.DateOfEvent}
                      avatar={<CalenderIcon></CalenderIcon>}
                    ></Chip>
                    <IconButton>
                      <CallIcon />
                    </IconButton>
                    <IconButton>
                      <MailIcon />
                    </IconButton>
                    <IconButton>
                      <LocationIcon />
                    </IconButton>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(Events);
