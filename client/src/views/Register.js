import React from "react";
import { withStyles } from "@material-ui/styles";
import {
  Paper,
  Grid,
  TextField,
  Chip,
  RadioGroup,
  FormControlLabel,
  Radio,
  InputLabel,
  Select,
  FormControl,
  Switch,
  Typography,
  Slide
} from "@material-ui/core";
import {
  KeyboardDatePicker,
  MuiPickersUtilsProvider
} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import ParallexMain from "../components/ParallexMain";
import TextInImage from "../components/TextInImage";
import PaperContainer from "../components/PaperContainer";
const styles = theme => ({
  root: {
    height: "100%"
  },
  form: {
    margin: "-60px 30px 0px"
  },
  paper: {
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    marginBottom: 24,
    padding: 16,
  },
  marginAuto: {
    marginLeft: "auto",
    marginRight: "auto"
  },
  chip: {
    marginBottom: -70,
    marginLeft: "auto"
  },
  registerButton: {
    display: "flex"
  },
  textPadding:{
      padding:'16px 24px'
  }
});
const date = new Date();
class Register extends React.Component {
  state = {
    switchEnabled: false
  };
  handleSwitch = event => {
    if (event.target.checked) {
      this.setState({ switchEnabled: true });
    } else {
      this.setState({ switchEnabled: false });
    }
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <ParallexMain Image="https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg">
            <TextInImage variant="h1" text="Come be a part of Us!"></TextInImage>
        </ParallexMain>
        <PaperContainer>
            <Typography className={classes.textPadding}  align="center" variant="body1"> Mahatma Gandhi had rightly said,<strong>”Be the change you wish to see in the world.” </strong>We often talk about so many unaddressed issues of the society but seldom act upon them. But what if we get an opportunity to do something for the betterment of our society? To change somebody’s life? To create a direct impact? 
Volunteering is one such platform which enables us to work selflessly towards a goal having a greater purpose in life. The Greek philosopher, Aristotle once said that the essence of life is to serve others and do good.Think of the happiness and joy you'll experience when you are able to help the ones in need. It is overwhelming because you suddenly lead a life of greater purpose and of selfless service which inturn evokes a feeling of well-being and immense satisfaction.
At Aasya Foundation, we’re dedicated to fighting back and helping people who are affected with different kinds of diseases and helping the needy and old people financially, emotionally and psychologically. We believe that small acts, when multiplied by many, can definitely transform the world. Come, be a part of this circle of creating the change! Let us come together to make a difference. Come, volunteer with us because the giving of one’s time and effort is the best gift one can give!</Typography>
            <Grid container>
              <Grid className={classes.marginAuto} item xs={12} md={10} sm={10}>
                <Grid container spacing={2}>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="FirstName"
                      fullWidth
                      label="First Name"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="FirstName"
                      fullWidth
                      label="Last Name"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="Email"
                      fullWidth
                      label="Email"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <RadioGroup row={true}>
                      <FormControlLabel
                        value="Male"
                        label="Male"
                        labelPlacement="end"
                        control={<Radio color="primary"></Radio>}
                      >
                        Male
                      </FormControlLabel>
                      <FormControlLabel
                        value="Female"
                        label="Female"
                        labelPlacement="end"
                        control={<Radio color="primary"></Radio>}
                      >
                        Female
                      </FormControlLabel>
                      <FormControlLabel
                        value="other"
                        label="Other"
                        labelPlacement="end"
                        control={<Radio color="primary"></Radio>}
                      >
                        Other
                      </FormControlLabel>
                    </RadioGroup>
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                      <KeyboardDatePicker
                        variant="inline"
                        inputVariant="outlined"
                        id="date"
                        format="MM/dd/yyyy"
                        label="Date of Birth"
                        size="small"
                        value={date}
                      />
                    </MuiPickersUtilsProvider>
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="phonenumber"
                      fullWidth
                      label="Phone Number"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="Profession"
                      fullWidth
                      label="Profession"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="state"
                      fullWidth
                      select
                      label="state"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="city"
                      fullWidth
                      select
                      label="City"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="currentcity"
                      fullWidth
                      select
                      label="Current City"
                    />
                  </Grid>
                  <Grid item xs={12} sm={6} md={6}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="bloodGroup"
                      fullWidth
                      select
                      label="Blood Group"
                    />
                  </Grid>
                  <Grid item xs={12} sm={12} md={12}>
                    <Grid container spacing={1}>
                      <Grid item>
                        <Typography variant="h6">
                          {" "}
                          Would this be your first NGO to work for ?
                        </Typography>
                      </Grid>
                      <Grid item>
                        <Typography variant="body1">Yes</Typography>
                      </Grid>
                      <Grid item>
                        <Switch
                          checked={this.state.switchEnabled}
                          size="small"
                          onChange={this.handleSwitch}
                        ></Switch>
                      </Grid>
                      <Grid item>
                        <Typography variant="body1"> No</Typography>
                      </Grid>
                    </Grid>
                  </Grid>
                  {this.state.switchEnabled && (
                    <Slide direction="left" in={this.state.switchEnabled}>
                      <Grid item xs={12} sm={12} md={12}>
                        <TextField
                          size="small"
                          variant="outlined"
                          name="previous"
                          fullWidth
                          multiline={true}
                          rows={2}
                          label="Previous Details"
                        />
                      </Grid>
                    </Slide>
                  )}
                  <Grid item xs={12} sm={12} md={12}>
                    <TextField
                      size="small"
                      variant="outlined"
                      name="whyaasya"
                      fullWidth
                      multiline={true}
                      rows={5}
                      label="Why aasya"
                      margin="normal"
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
            <div className={classes.registerButton}>
              <Chip clickable={true} className={classes.chip} color="primary" label="Register" />
            </div>
        </PaperContainer>
      </div>
    );
  }
}

export default withStyles(styles)(Register);
