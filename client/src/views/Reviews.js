import React from 'react'
import ParallexMain from '../components/ParallexMain'
import TextInImage from '../components/TextInImage';
import Reviewpage from '../components/reviews/Review-Page';

class Reviews extends React.Component{
    render(){
        return(
        <div style={{height:'100%'}}>
            <ParallexMain Image="https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg">
                <TextInImage variant="h1" text="Who people say about us ?"> </TextInImage>
            </ParallexMain>
            <Reviewpage/>
        </div>
        );
    }
}

export default Reviews