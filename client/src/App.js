import React ,{ lazy, Suspense } from 'react';
import { ThemeProvider, CssBaseline } from '@material-ui/core';
import { Switch, HashRouter } from 'react-router-dom';
import ReactLoading from 'react-loading';
import RouteWithLayout from './RouteWithLayout';
import { theme } from './theme' ;
import MainLayout from './layout/Main';

import './App.css';

const HomePage = lazy( () => import('./views/HomePage'))
const Events = lazy(() => import('./views/Events'))
const ContactUs = lazy(() => import('./views/ContactUs'))
const AboutUs = lazy(() => import('./views/AboutUs'))
const Register = lazy(() => import('./views/Register'))
const Reviews = lazy(() => import('./views/Reviews'))
const Gallery = lazy(() => import('./views/Gallery'))
const SuccessStories = lazy(() => import('./views/SuccessStories'))
const SuccessStoryAdd = lazy(() => import('./contentportal/SuccessStoryAdd'))
const AdminMain = lazy(() => import('./layout/adminlayout/AdminMain'))
const MediaRoom = lazy(() => import('./views/MediaRoom'))


class App extends React.Component{
    render(){
        return(
            <ThemeProvider theme ={theme}>
                <CssBaseline/>
                <HashRouter >
                <Suspense fallback={<ReactLoading type={"balls"} color={'black'}/>}>
                    <Switch>
                        <RouteWithLayout exact path = "/"  layout={MainLayout} component={HomePage}/>
                        <RouteWithLayout exact path = "/events"  layout={MainLayout} component={Events}/>
                        <RouteWithLayout exact path = "/contact" layout={MainLayout} component={ContactUs}/>
                        <RouteWithLayout exact path = "/about" layout={MainLayout} component={AboutUs}/>
                        <RouteWithLayout exact path = "/register" layout={MainLayout} component={Register}/>
                        <RouteWithLayout exact path = "/success-stories" layout={MainLayout} component={SuccessStories}/>
                        <RouteWithLayout exact path = "/success-stories/:storyid" layout={MainLayout} component={SuccessStories}/>
                        <RouteWithLayout exact path = "/reviews" layout={MainLayout} component={Reviews}/>
                        <RouteWithLayout exact path = "/gallery" layout={MainLayout} component={Gallery}/>
                        <RouteWithLayout exact path = "/media" layout={MainLayout} component={MediaRoom}/>
                        <RouteWithLayout exact path = "/addStory" layout={AdminMain} component={SuccessStoryAdd}/>
                    </Switch>
                </Suspense>
                </HashRouter>
            </ThemeProvider>
        )
    }
}

export default App