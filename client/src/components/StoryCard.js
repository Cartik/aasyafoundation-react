import React, { Fragment } from "react";
import {
  CardActions,
  Button,
  Card,
  Chip,
  CardMedia,
  CardContent,
  Collapse,
  Typography,
} from "@material-ui/core";
import RightArrow from "@material-ui/icons/KeyboardArrowRight";
import { withStyles } from "@material-ui/styles";
import Dotdotdot from 'react-dotdotdot'

const styles = (theme) => ({
  card: {
    overflow: "initial",
    borderRadius: theme.spacing(2),
    padding: theme.spacing(2),
  },
  media: {
    height: 0,
    paddingTop: "56.25%", // 16:9
  },
  innerCard: {
    borderRadius: theme.spacing(1),
    marginTop: "-30px",
  },
  chip: {
    position: "absolute",
    marginLeft: "-30px",
    marginTop: "10px",
    opacity: 0.9,
    textTransform: "capitalize",
    borderRadius: theme.spacing(0.5),
  },
});
class StoryCard extends React.Component {
  state = {
    checked: false,
  };
  handleReadMore = () => {
    this.setState({ checked: !this.state.checked });
  };
  render() {
    const { classes, mainStory } = this.props;
    console.log(mainStory);
    return (
      <Fragment>
        {
          mainStory.hasOwnProperty("name") && <Card className={classes.card}>
          <Chip
            className={classes.chip}
            color="secondary"
            label={mainStory.name}
          />
          <Card className={classes.innerCard} raised>
            <CardMedia className={classes.media} image={process.env.REACT_APP_STRAPI_URL+mainStory.image.url} />
          </Card>
          <CardContent>
            {this.state.checked ? <Typography variant="body1">
                {mainStory.description}
              </Typography> : <Dotdotdot clamp={6}>
              <Typography variant="body1">
                {mainStory.description}
              </Typography>
              </Dotdotdot>}
            <Collapse in={this.state.checked} >
            
            </Collapse>
          </CardContent>
          <CardActions>
            {!this.state.checked && (
              <Button
                onClick={this.handleReadMore}
                endIcon={<RightArrow></RightArrow>}
                size="small"
              >
                Read more
              </Button>
            )}
            {this.state.checked && (
              <Button
                onClick={this.handleReadMore}
                endIcon={<RightArrow></RightArrow>}
                size="small"
              >
                Read Less
              </Button>
            )}
          </CardActions>
        </Card>
        }
      </Fragment>
      
    );
  }
}

export default withStyles(styles)(StoryCard);
