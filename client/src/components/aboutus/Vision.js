import React from 'react';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const Vision = () => {

  const vision =
  "Aasya foundation envisions a future where no one should go through such arduous journeys alone. We aspire to be an outgrown organization dedicated to fighting social disparities. Our purpose is to improve the quality of people's lives by empowering them about the bewildering intricacies of life.We embrace the diversity of our team as we continue to grow and educate people about the difficulties of being able to cope up with life-challenging diseases and Environmental problems.";

  return (
    <React.Fragment>
      <Grid container justify="center">
        <Grid item xs={12} sm={8}>
          <Typography component="h2" variant="h2" align="center">Our Vision</Typography>
          <Typography component="p" variant="body1" align="center">{vision}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default Vision;
