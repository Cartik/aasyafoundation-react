import React from 'react';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

function AboutAasya() {

  const aboutAasya =
  "Aasya is one of Indias independent youth volunteer non-profit organization. Aasya, as a platform, enables over 1000 volunteers across 10 cities in India for causes like education, environment, health and community welfare. Sustainability, social equality, and the environment are now business problems. And corporate leaders can't depend on governments to solve them - Inspired by Senge's philosophy, a group of young people came together in 2017 to set up Aasya Foundation to initially work together to help the cancer kids and later on by understanding various situations it thus started to work with by taking up the grassroots initiatives for effecting positive changes in the lives of underprivileged children, their families, and communities... We are strongly committed to educating people also help them render in their problems to get a sustainable living in a way to instill integrity, leadership and hence other qualities in individuals to promote a safe and ethical environment.";

  return (
    <React.Fragment>
      <Grid container justify="center">
        <Grid item xs={12} sm={8}>
          <Typography component="h2" variant="h2" align="center">Who we are</Typography>
          <Typography component="p" variant="body1" align="center">{aboutAasya}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default AboutAasya;
