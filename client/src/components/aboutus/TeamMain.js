import React from "react";
import { withStyles } from "@material-ui/styles";
import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  IconButton,
  Button,
  Divider,
  Zoom,
} from "@material-ui/core";
import classNames from "classnames";
import CallIcon from "@material-ui/icons/Call";
import Mail from "@material-ui/icons/Mail";
import FaceBookIcon from "@material-ui/icons/Facebook";

const styles = theme => ({
  teamDivMainContainer: {
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    paddingLeft: 16,
    paddingRight: 16,
    textAlign: "center"
  },
  marginL: {
    marginLeft: "auto"
  },
  marginR: {
    marginRight: "auto"
  },
  paddingLeft: {
    paddingLeft: 16
  },
  paddingRight: {
    paddingRight: 16
  },
  teamGridContainer: {
    marginLeft: "-15px",
    marginRight: "-15px",
    paddingTop: 60,
  },
  avatar: {
    width: theme.spacing(16),
    height: theme.spacing(16)
  },
  teamMemCard: {
    boxShadow: "none",
    backGround: "transparent"
  },
  media: {
    height: 140,
    width: 140,
    borderRadius: "50%",
    boxShadow: "0 16px 38px -12px rgba(0, 0, 0, 0.56)"
  },
  boardMemName: {
    fontWeight: 700,
    fontSize: "1.5rem",
    marginTop: 16
  },
  cardContent: {
    paddingBottom: 0
  },
  justifyCenter: {
    justifyContent: "center"
  },
  cardContainer: {
    boxShadow: "none",
    backGround: "transparent",
    display: "flex",
    flexDirection: "row"
  },
  cardMedia: {
    height: 100,
    width: 100,
    borderRadius: theme.spacing(1)
  },
  removePadding: {
    paddingTop: 0,
    paddingBottom: 0
  }
});

const memberList = [
  {
    name: "Sree Ram",
    designation: "Founder",
    description:
      'We all know that, humans are interdependent, the support of family , community and others are of much importance. From ? womb to the tomb ?we all rely on the help and services of others."',
    image:
      "https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg"
  },
  {
    name: "Madhukar",
    designation: "Founder",
    description:
      'We all know that, humans are interdependent, the support of family , community and others are of much importance. From ? womb to the tomb ?we all rely on the help and services of others."',
    image:
      "https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg"
  },
  {
    name: "Niharika",
    designation: "Founder",
    description:
      'We all know that, humans are interdependent, the support of family , community and others are of much importance. From ? womb to the tomb ?we all rely on the help and services of others."',
    image:
      "https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg"
  },
  {
    name: "Emmadi Shiva",
    designation: "Founder",
    description:
      'We all know that, humans are interdependent, the support of family , community and others are of much importance. From ? womb to the tomb ?we all rely on the help and services of others."',
    image:
      "https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg"
  }
];
const boardMemberList = [
  { id: 1, name: "Karthik", imageUrl: "", designation: "Board Member" },
  { id: 1, name: "Karthik", imageUrl: "", designation: "Board Member" },
  { id: 1, name: "Karthik", imageUrl: "", designation: "Board Member" }
];
class TeamMain extends React.Component {
  state = {
    displayBoard: false
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.teamDivMainContainer}>
        <Grid
          container
          xs={12}
          sm={12}
          md={10}
          className={classNames(
            classes.paddingLeft,
            classes.paddingRight,
            classes.marginL,
            classes.marginR
          )}
          justify="center"
        >
          <Typography
            className={classes.header}
            gutterBottom
            variant="h4"
            align="center"
          >
            Our Team
          </Typography>
          <Grid container className={classes.teamGridContainer}>
            {memberList.map(member => (
              <Grid item xs={12} sm={3} md={3} justify="center">
                <Card className={classes.teamMemCard}>
                  <CardMedia
                    className={classNames(
                      classes.media,
                      classes.marginL,
                      classes.marginR
                    )}
                    image={member.image}
                  ></CardMedia>
                  <CardContent className={classes.cardContent}>
                    <Typography gutterBottom variant="h5">
                      {member.name}
                    </Typography>
                    <Typography gutterBottom variant="h6">
                      {" "}
                      {member.designation}
                    </Typography>
                    <Typography gutterBottom variant="body2">
                      {" "}
                      {member.description}
                    </Typography>
                  </CardContent>
                  <CardActions className={classes.justifyCenter} disableSpacing>
                    <IconButton>
                      <CallIcon />
                    </IconButton>
                    <IconButton>
                      <Mail />
                    </IconButton>
                    <IconButton>
                      <FaceBookIcon />
                    </IconButton>
                  </CardActions>
                </Card>
              </Grid>
            ))}
          </Grid>
        </Grid>
        <Button
          variant="contained"
          size="small"
          onClick={() => this.setState({ displayBoard: true })}
        >
          View Entire Team
        </Button>
        {this.state.displayBoard && (
          <Grid container className={classes.teamGridContainer} spacing={3}>
            {boardMemberList.map((member,index) => {
              return (
              <Zoom in={this.state.displayBoard} style={{ transitionDelay: this.state.displayBoard ? (300*index)+'ms' : '0ms' }}>
              <Grid item xs={12} sm={6} md={4} className= {classNames(classes.marginL,classes.marginR)}>
                <Card className={classes.cardContainer}>
                  <CardMedia
                    className={classes.cardMedia}
                    image="https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg"
                  ></CardMedia>
                  <div>
                    <CardContent className={classes.removePadding}>
                      <Typography variant="subtitle1">
                        Karthik Uppala
                      </Typography>
                      <Typography gutterBottom variant="body1">
                        Founder
                      </Typography>
                    </CardContent>
                    <Divider variant="middle" />
                    <CardActions
                      className={classes.removePadding}
                      disableSpacing
                    >
                      <IconButton>
                        <CallIcon fontSize="small" />
                      </IconButton>
                      <IconButton>
                        <Mail fontSize="small" />
                      </IconButton>
                      <IconButton>
                        <FaceBookIcon fontSize="small" />
                      </IconButton>
                    </CardActions>
                  </div>
                </Card>
              </Grid>
            </Zoom>
              )
            })}
          </Grid>
        )}



      </div>
    );
  }
}

export default withStyles(styles)(TeamMain);
