import React from 'react'
import classNames from 'classnames'
import { Typography, Grid, withStyles, Card, CardMedia } from '@material-ui/core';
import grey from "@material-ui/core/colors/grey";
const styles = theme => ({
    themeDiv: {
        width: "100%",
        marginLeft: "auto",
        marginRight: "auto",
        paddingLeft: 16,
        paddingRight: 16
      },
      marginL: {
        marginLeft: "auto"
      },
      marginR: {
        marginRight: "auto"
      },
      themeDataDiv: {
        paddingTop: 32,
        paddingBottom: 16
      },
      paddingLeft: {
        paddingLeft: 16
      },
      paddingRight: {
        paddingRight: 16
      },
      media: {
        height: 0,
        paddingTop: "60%",
      },
      themeGridContainer: {
        width: "auto",
        marginLeft: "-15px",
        marginRight: "-15px",
        paddingTop:24
      },
})

const themeList = [
    {
      imageUrl:
        "https://cdn.pixabay.com/photo/2019/04/29/07/38/blood-donation-4165394_1280.jpg",
      themeName: "BLOOD CIRCLE",
      contentHeading: "It’s a beautiful day to save lives.",
      contentSubData:
        "One pint of blood can save three lives. You can be the one saving three lives by donating blood. Literally, anyone of us who is healthy and is willing to share our little amount of blood can give a second life to a person; someone we don’t even know. And that is the real act of humanity. Blood circle by aasya is here to contribute our part by bringing both the donor and the recipient on a single platform and embark the noble cause to fulfill the requisite of the needy."
    },
    {
      imageUrl:
        "https://cdn.pixabay.com/photo/2015/07/28/22/05/child-865116_1280.jpg",
      themeName: "GYANA KOSHA",
      contentHeading: "Let your dreams be our wings.",
      contentSubData:
        "Give a man a fish and you feed him for a day; teach a man to fish and you feed him for a lifetime.- We are firm believers of this saying.  Gyana kosha, as a team embodies the fact that education changes the world. Education is the powerful approach to have a better perspective of life and a better life itself. We believe that when a person is educated, the knowledge they gain is conveyable to others leading to the overall well being of a family and subsequently the society. Gyana kosha, through aasya is our attempt at making this happen on a elementary level by providing educational resources to the unprivileged in every way possible."
    },
    {
      imageUrl:
        "https://cdn.pixabay.com/photo/2013/07/12/19/24/sapling-154734_1280.png",
      themeName: "MAHI",
      contentHeading: "He who plants a tree plants a hope.",
      contentSubData:
        "'Mahi' is the unity where the planet is best served just by sowing a seed. The importance of trees must never be overlooked. Considering the same, the key objective of the Aasyans in the name of Mahi is being fulfilled through plantations and by broadening the awareness among people. This is an efficacious cause and more the number of people join us, the more effective our act could be. \"Let's sow the thought of peace and feed the need of woods to our environment.\" This is our simplest effort to do the same and turn our earth greener sooner."
    },
    {
      imageUrl:
        "https://cdn.pixabay.com/photo/2015/07/15/07/13/homeless-845752_1280.jpg",
      themeName: "Anandadhan",
      contentHeading: "An opportunity to exercise your privilege of being a mother to the world.",
      contentSubData:
        "Food is the key necessity a human being has. It is as important as the oxygen we breathe. But, on the contrary, unlike oxygen everybody is not conferred with food. Mostly, poverty is ceased to be the root for hunger. Anandadhan, an initiative by aasya spares no effort to make sure that everyone is provided with food, the food that comes for the people who otherwise would not be able to afford it, the food that just doesn't meet the hunger, but also helps them stay nourished. With our earnest effort, even if one out the many million people is full and doesn't sleep with an empty stomach, that would very much meet our objective. No body should be deprived off their basic necessities and  through anandadan, we try to ensure that happens."
    },
    {
      imageUrl:
        "https://cdn.pixabay.com/photo/2016/04/12/03/15/acrylic-1323646_1280.jpg",
      themeName: "Women Empowerment",
      contentHeading: "You’re the woman and that’s your weapon.",
      contentSubData:
        "Women - the source of power; power to cope; power for resilience, personification of patience. Everything a woman is, is everything the world needs. When called for, she could be everything. She could be the one who devotes her life to the world with kindness; also she could be the one who fiercely fights against repression. We trust that women being the incredible creatures they are, are every bit worthy to have the benefit to empower themselves in terms of equal opportunity, seeing beyond bias, and sway to their perfect tune in every aspect and hold up the sky just like always. Women empowerment department through aasya is our simplest effort to attune the world around for the same to ensue!"
    }
  ];
class ThemeMain extends React.Component{
    
    render(){
        const {classes} = this.props;
         return(
            <div className={classes.themeDiv}>
            <Grid container className={classes.themeGridContainer}>
              <Grid
                item
                xs={12}
                sm={12}
                md={10}
                className={classNames(
                  classes.paddingLeft,
                  classes.paddingRight,
                  classes.marginL,
                  classes.marginR
                )}
              >
                <Typography
                  gutterButtom
                  gutterBottom
                  variant="h3"
                >
                  What we do
                </Typography>
                <div className={classes.themeDataDiv}>
                  {themeList.map((theme, index) => {
                    if (index % 2 === 0) {
                      return (
                        <Grid container className={classes.themeGridContainer}>
                          <Grid
                            className={classNames(
                              classes.paddingLeft,
                              classes.paddingRight
                            )}
                            item
                            xs={12}
                            sm={12}
                            md={5}
                          >
                            <Card raised={true}>
                              <CardMedia
                                className={classes.media}
                                image= {theme.imageUrl}
                              />
                            </Card>
                          </Grid>
                          <Grid
                            className={classNames(
                              classes.paddingLeft,
                              classes.paddingRight
                            )}
                            item
                            xs={12}
                            sm={12}
                            md={7}
                          >
                            <Typography
                              color="primary"
                              className={classes.themeName}
                            >
                              {theme.themeName}
                            </Typography>
                            <Typography
                              gutterBottom
                              variant="h5"
                            >
                              {theme.contentHeading}
                            </Typography>
                            <Typography
                              variant="body1"
                            >
                              {theme.contentSubData}
                            </Typography>
                          </Grid>
                        </Grid>
                      );
                    } else {
                        return (
                            <Grid container className={classes.themeGridContainer}>
                                <Grid 
                            className={classNames(
                              classes.paddingLeft,
                              classes.paddingRight
                            )}
                            item
                            xs={12}
                            sm={12}
                            md={7}
                          >
                            <Typography
                              color="primary"
                            >
                             {theme.themeName}
                            </Typography>
                            <Typography
                              gutterBottom
                              variant="h5"
                            >
                              {theme.contentHeading}
                            </Typography>
                            <Typography
                              variant="body1"
                            >
                              {theme.contentSubData}
                            </Typography>
                          </Grid>
                          <Grid
                            className={classNames(
                              classes.paddingLeft,
                              classes.paddingRight
                            )}
                            item
                            xs={12}
                            sm={12}
                            md={5}
                          >
                            <Card raised={true}>
                              <CardMedia
                                className={classes.media}
                                image={theme.imageUrl}
                              />
                            </Card>
                          </Grid>
                          
                        </Grid>
                        )
                    }
                  })}
                </div>
              </Grid>
            </Grid>
          </div>
        )
    }
}

export default withStyles(styles) (ThemeMain)