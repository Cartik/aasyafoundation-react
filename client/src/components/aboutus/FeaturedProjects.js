import React from "react";
import { withStyles } from "@material-ui/styles/";
import {
  Grid,
  Typography,
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Button,
  CardHeader,
  Avatar,
  IconButton,
  Slide
} from "@material-ui/core";
import ReactCardFlip from "react-card-flip";
import BackIcon from "@material-ui/icons/ArrowForward"

const styles = theme => ({
  mainContainer: {
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    paddingLeft: 16,
    paddingRight: 16
  },
  gridItem: {
    paddingLeft: 16,
    paddingRight: 16,
    marginLeft: "auto",
    marginRight: "auto"
  },
  gridContainer: {
    width: "auto",
    marginRight: -16,
    marginLeft: -16,
    paddingTop: 24
  },
  gridProjectsContainer: {
    paddingTop: 56
  },
  cardRoot: {
    borderRadius: theme.spacing(0.5),
    transition: "0.3s",
    display: "flex",
    marginBottom: theme.spacing(3),
    flexDirection: "column",
    overflow: "initial",
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
    }
  },
  cardMedia: {
    width: "88%",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: theme.spacing(-3),
    borderRadius: theme.spacing(1),
    paddingBottom: "48%"
  },
  cardContent: {
    maxHeight: 250,
    overflow: "hidden"
  },
  cardContentTextAlign: {
    textAlign: "center"
  },
  cardActions: {
    justifyContent: "space-around"
  }
});

const featuredProjects = [
  {
    id:1,
    title: "Mission 500",
    description:
      'To Save 500 cancer suffering children\'s within 10 years. To conduct 500 health camps in rural areas of our country. To conduct 500 cancer awareness programs at schools, colleges, Slums, Rural areas of our country.Conducting "ANANDHAAN"( feeding empty stomach \'s ) programs In public and remote areas of our country.To construct An old-age home in order to provide shelter for Old-age people.Mission "MAHI" - To plant 1 crore of plants in our country in order to balance Environmental Hazards.',
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg"
  },
  {
    id:2,
    title: "Aasya Oldage Home",
    description:'In this domain of infinite emotions, Two irreplaceable emotion\'s are mother\'s love and father\'s care. We cannot payback for their love and care throughout our span of life. Simply,They are beyond the bounds of possibility.Love of our grandparents is remembered not in their wrinkles but in the laughter and tears of their children and their children\'s children. For them Growing old is compulsory, growing up is optional. For us caring them is responsibility, loving them is competence.',
    imageUrl:
      "https://cdn.pixabay.com/photo/2016/10/27/14/23/poor-1775239_1280.jpg"
  }
];

class FeaturedProjects extends React.Component {
    state ={
        slide:false
    }
    handleReadMore = (projectId) => {
        this.setState({[projectId]:true})
    }
    handleReadMoreBack = (projectId) => {
        this.setState({[projectId]:false})
    }
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.mainContainer}>
        <Grid container className={classes.gridContainer}>
          <Grid item xs={12} sm={10} md={10} className={classes.gridItem}>
            <Typography variant="h3">FeaturedProjects</Typography>
            <Grid
              container
              className={classes.gridProjectsContainer}
              spacing={4}
            >
              {featuredProjects.map(project => (
                <Grid item xs={12} sm={6} md={6}>
                  <ReactCardFlip isFlipped={this.state[project.id] === undefined ? false : this.state[project.id]}>
                    <Card raised={true} className={classes.cardRoot}>
                      <CardMedia
                        className={classes.cardMedia}
                        image={project.imageUrl}
                      ></CardMedia>
                      <CardContent className={classes.cardContent}>
                        <Typography gutterBottom color="secondary" variant="subtitle1">
                          {project.title}
                        </Typography>
                        <Typography
                          className={classes.cardContentTextAlign}
                          variant="body1"
                        >
                          {project.description}
                        </Typography>
                      </CardContent>
                      <CardActions className={classes.cardActions}>
                        <Button
                          size="small"
                          variant="outlined"
                          color="secondary"
                          onClick ={ () => this.handleReadMore(project.id)}
                        >
                          Read More
                        </Button>
                      </CardActions>
                    </Card>
                    <Card>
                      <CardHeader
                        avatar={
                          <Avatar
                            src={project.imageUrl}
                            sizes="small"
                          ></Avatar>
                          
                        }
                        disableTypography = {true} 
                    title = {
                    <Typography color="secondary" variant="subtitle1">
                        {project.title}
                    </Typography>
                    }
                        action={
                            <IconButton aria-label="back" onClick={()=> this.handleReadMoreBack(project.id)}>
                              <BackIcon/>
                            </IconButton>
                          }
                      ></CardHeader>
                      <CardContent>
                        <Typography className={classes.cardContentTextAlign}variant="body1">
                          {project.description}
                        </Typography>
                      </CardContent>
                    </Card>
                  </ReactCardFlip>
                </Grid>
              ))}
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(FeaturedProjects);
