import React, { Component } from 'react'

import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import CardActions from '@material-ui/core/CardActions';
import IconButton from '@material-ui/core/IconButton';
import CallIcon from "@material-ui/icons/Call";
import Mail from "@material-ui/icons/Mail";
import FaceBookIcon from "@material-ui/icons/Facebook";

import { withStyles } from "@material-ui/styles";

const leaders = [
  {
    name: 'Sreeram',
    designation: 'Founder',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio recusandae doloribus ut fugit officia voluptate soluta.',
    imageUrl: 'https://demos.creative-tim.com/material-kit-pro-react/static/media/marc.101b9dc3.jpg'
  }, {
    name: 'Madhukar',
    designation: 'President',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio recusandae doloribus ut fugit officia voluptate soluta.',
    imageUrl: 'https://demos.creative-tim.com/material-kit-pro-react/static/media/marc.101b9dc3.jpg'
  }, {
    name: 'Niharika',
    designation: 'Working President',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio recusandae doloribus ut fugit officia voluptate soluta.',
    imageUrl: 'https://demos.creative-tim.com/material-kit-pro-react/static/media/marc.101b9dc3.jpg'
  }, {
    name: 'Emmadi Shiva',
    designation: 'Working President',
    description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae aut minima nihil sit distinctio recusandae doloribus ut fugit officia voluptate soluta.',
    imageUrl: 'https://demos.creative-tim.com/material-kit-pro-react/static/media/marc.101b9dc3.jpg'
  }
]

const styles = () => ({
  cardImage: {
    height: '12rem',
    width: '12rem',
    borderRadius: '0.6rem',
    boxShadow: "0 6px 5px 0px rgba(0, 0, 0, 0.2)",
    margin: '2rem auto 0'
  },
  cardActions: {
    padding: 0,
    justifyContent: 'center'
  }
})

class Leadership extends Component {

  constructor(props) {
    super(props);
    this.state = {
      
    }
  }

  render() {

    const { classes } = this.props;

    return (
      <React.Fragment>
        <Container maxWidth="xl">
          <Typography component="h2" variant="h2" align="center">Leadership</Typography>
          <Grid container justify="center">
            {
              leaders.map((member, index) => (
                <Grid item xs={12} sm={6} md={3} key={index}>
                  <Card elevation={0}>
                    <CardMedia image={member.imageUrl} className={classes.cardImage}></CardMedia>
                    <CardContent>
                      <Typography gutterBottom align="center" variant="h5">{member.name}</Typography>
                      <Typography gutterBottom align="center" variant="h6">{member.designation}</Typography>
                      <Typography gutterBottom align="center" variant="body2">{member.description}</Typography>
                    </CardContent>
                    <CardActions disableSpacing className={classes.cardActions}>
                      <IconButton> 
                        <CallIcon /> 
                      </IconButton>
                      <IconButton> 
                        <Mail /> 
                      </IconButton>
                      <IconButton> 
                        <FaceBookIcon /> 
                      </IconButton>
                    </CardActions>
                  </Card>
                </Grid>
              ))
            }
          </Grid>
        </Container>
      </React.Fragment>
    )
  }
}

export default withStyles(styles)(Leadership);

