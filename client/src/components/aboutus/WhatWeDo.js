import React from 'react';

import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const WhatWeDo = () => {

  const whatWeDo = "The idea behind Aasya is to help poverty-stricken people who are fighting with life challenging diseases by providing necessary financial support and stand by them emotionally. We connect to the people who care, with the causes that matter. We are committed lot in providing health care-services to the economically backward classes in India. We engage with all types problems related to health, education, character development, physically and mentally challenging survivors. After all, being able to care someone helps us to live a life worthwhile. Believing in what we do and what we wish is our main agenda in creating the hype in today?s scenario.";

  return (
    <React.Fragment>
      <Grid container justify="center">
        <Grid item xs={12} sm={8}>
          <Typography component="h2" variant="h2" align="center">What We Do</Typography>
          <Typography component="p" variant="body1" align="center">{whatWeDo}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  )
}

export default WhatWeDo;
