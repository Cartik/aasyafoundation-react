import React from "react";

import { makeStyles } from '@material-ui/core/styles';

import PaperContainer from "../PaperContainer";
import AboutAasya from './AboutAasya';
import Vision from './Vision';
import WhatWeDo from './WhatWeDo';
// import FeaturedProjects from "./FeaturedProjects";
// import ThemeMain from './ThemeMain'
// import TeamMain from "./TeamMain";
import Leadership from "./Leadership";
import Register from "./Register";

const useStyles = makeStyles(() => ({
  aboutContainer: {
    paddingTop: 70
  },
  sectionSpacing: {
    margin: '6rem 0'
  }
}));


const AboutUsMain = () => {

  const classes = useStyles();

  return (
    <PaperContainer>
      <div className={classes.aboutContainer}>
        <AboutAasya />
      </div>
      <div className={classes.sectionSpacing}>
        <WhatWeDo />
      </div>
      <div className={classes.sectionSpacing}>
        <Vision />
      </div>
      <div className={classes.sectionSpacing}>
        <Leadership />
      </div>
      
      {/* <div className={classes.sectionSpacing}>
        <FeaturedProjects></FeaturedProjects>
      </div> */}
      {/* <div className={classes.sectionSpacing}>
        <ThemeMain></ThemeMain>
      </div> */}
      {/* <div className={classes.sectionSpacing}>
        <TeamMain></TeamMain>
      </div> */}
      <div className={classes.sectionSpacing}>
        <Register></Register>
      </div>
    </PaperContainer>
  );

}

export default AboutUsMain;
