import React from 'react'
import {withStyles} from '@material-ui/styles'
import { Typography, Button } from '@material-ui/core';
const styles = theme =>({
	root: {
		width: "100%",
		marginLeft: "auto",
		marginRight: "auto",
		paddingLeft: 16,
		paddingRight: 16,
		marginBottom:24,
		textAlign:'center'
	},
	paddingBottom:{
		paddingBottom:30
	}
})
class Register extends React.Component{
	render(){
		const {classes} = this.props;
		return(
			<div className={classes.root}>
				<Typography gutterBottom align="center" variant="h3"> Want to join with us ? </Typography>
				<Typography className={classes.paddingBottom} align="center" variant="body1"> We need to write some content needed regarding the foundation to make the volunteer register</Typography>
				<div className={classes.paddingBottom}>
					<Button size="large" color="primary" variant="contained">Join Us</Button>
				</div>
			</div>
		)
	}
}

export default withStyles(styles) (Register)