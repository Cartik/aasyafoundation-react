import React from "react";
import { Snackbar } from "@material-ui/core";
import { SnackbarContent } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';
import { amber, green } from '@material-ui/core/colors';
import ClassNames from 'classnames';
import {withStyles} from '@material-ui/styles'


const variantIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

const styles = theme =>({
    success: {
        backgroundColor: green[600],
      },
      error: {
        backgroundColor: theme.palette.error.dark,
      },
      info: {
        backgroundColor: theme.palette.primary.main,
      },
      warning: {
        backgroundColor: amber[700],
      },
      icon: {
        fontSize: 20,
      },
      iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
      },
      message: {
        display: 'flex',
        alignItems: 'center',
      },
})


class SnackBar extends React.Component {

  render() {
    const { classes, open,message , variant ,onClose} = this.props;
    const Icon = variantIcon[variant]
    return (
      <Snackbar
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "left",
        }}
        open={open}
        autoHideDuration={3000}
        onClose={onClose}
      >
        <SnackbarContent
          className={classes[variant]}
          message={
            <span id="aasya-snackbar" className={classes.message}>
              <Icon className={ClassNames(classes.icon, classes.iconVariant)} />
              {message}
            </span>
          }
        ></SnackbarContent>
      </Snackbar>
    );
  
}
}

export default withStyles(styles)(SnackBar)
