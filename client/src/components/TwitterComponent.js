import React from 'react'
import {TwitterTimelineEmbed} from 'react-twitter-embed'

class TwitterComponent extends React.Component{
    render(){
        return(
            <TwitterTimelineEmbed
            sourceType="profile"
            screenName="aasyafoundation"
            options={{height: 400}}/>
        )
    }
}
export default TwitterComponent