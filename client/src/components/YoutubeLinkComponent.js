import React from 'react'
import { Badge, Button } from '@material-ui/core'
import YoutubeIcon from '@material-ui/icons/YouTube'
import { withStyles } from "@material-ui/styles";

const styles = theme => ({
    buttonColor:{
        backgroundColor:'#ff0404db'
    }
})

class YoutubeLinkComponent extends React.Component{
    render(){
        const {classes} = this.props;
        return(
            <Badge anchorOrigin={{
                vertical: 'top',
                horizontal: 'left',
              }}    badgeContent={<YoutubeIcon></YoutubeIcon>}>
                <Button target="_blank" href="https://www.youtube.com/channel/UCT-L00Au3qEYMngNLI0NyLw"className={classes.buttonColor} variant="contained">Watch us on Youtube</Button>
            </Badge>
        )
    }
}

export default withStyles(styles)(YoutubeLinkComponent)