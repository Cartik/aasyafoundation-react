import React from 'react'
import { Paper } from '@material-ui/core';
import { withStyles } from '@material-ui/styles'

const styles = theme => ({
    paperMainContainer: {
        margin: "-80px 30px 0px",
        position: 'relative',
        zIndex: 2
    }
})
class PaperContainer extends React.Component{

    render(){

        const {classes , children} = this.props;

        return(
            <div className={classes.paperMainContainer}>
                <Paper elevation={3}>
                    {children}
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(PaperContainer)