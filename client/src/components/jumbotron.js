import React, { Fragment } from 'react';

import { makeStyles } from '@material-ui/core/styles';

import classNames from 'classnames';

const useStyles = makeStyles((theme) => ({
	jumbotronStyles: {
		display: 'flex',
		alignItems: 'center',
		flexDirection: 'column',
		padding: '3.5rem 0',
		justifyContent: 'center',
		backgroundColor: '#E0E0E0',
		width: '100%',
		height: 'auto',
		backgroundRepeat: 'no-repeat',
		backgroundPosition: 'center',
		backgroundSize: 'cover',
		position: 'relative',
		backgroundAttachment: 'fixed',

		'&::before': {
			width: '100%',
			height: '100%',
			position: 'absolute',
			content: "''",
			top: 0,
			left: 0,
			background: 'rgba(0,0,0,0.5)'
		}
	},
	jumbotronMedium: {
		height: '60vh',
		maxHeight: '50rem'
	},
	jumbotronFullScreen: {
		height: '100vh',
		maxHeight: '70rem'
	}
}));

const Jumbotron = (props) => {
	const classes = useStyles();

	const { children, imageURL, size } = props;

	return (
		<Fragment>
			<div 
				className={classNames(
					classes.jumbotronStyles,
					{
						[classes.jumbotronFullScreen] : size === 'fullScreen',
						[classes.jumbotronMedium] : size === 'medium'
					}
				)}
				style={{backgroundImage: `url(${imageURL})`}}
				>
				{children}
			</div>
		</Fragment>
	);
}

export default Jumbotron;