import React, { Children } from "react";
import { withStyles } from "@material-ui/styles";
import Pulse from "react-reveal/Pulse";

const styles = (theme) => ({
  parallexContainer: {
    overflow: "hidden",
    height: "65vh",
    maxHeight: "650px",
    zIndex: -1,
    "& div": {
      "&:hover": {
        transform: "scale(1.1)",
      },
    },
  },
  parallexDiv: {
    height: "100%",
    display: "flex",
    alignItems: "center",
    backgroundSize: "cover",
    justifyContent:"center",
    transition: "transform .8s",
  },
});
class ParallexMain extends React.Component {
  render() {
    const { classes, Image, children } = this.props;
    return (
      <Pulse>
        <div className={classes.parallexContainer}>
          <div
            className={classes.parallexDiv}
            style={{
              backgroundImage: `linear-gradient(rgba(0,0,0,0.7),rgba(0,0,0,0.7)),url(${Image})`,
            }}
          >
            {children}
          </div>
        </div>
      </Pulse>
    );
  }
}
export default withStyles(styles)(ParallexMain);
