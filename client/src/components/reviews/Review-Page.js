import React, { useState } from "react";
import Review from "./Cta-review";
import styled from "styled-components";
import { Card, CardContent } from "@material-ui/core";
import ParallexMain from "../ParallexMain";
import PaperContainer from "../PaperContainer";

const StyledPage = styled("div")`
  background: linear-gradient(#e66465, #9198e5);
  height: 100vh;
  display: flex;
  flex-direction: row;
  justify-content: center;
  /* align-items: stretch; */
  padding: 3rem;
  flex-wrap: wrap;

  .alignment {
    height: auto;
    flex: 0 0 40%;
    margin: 1rem;
    /* height: 100px; */
  }

  p {
    font-size: 1rem;
    font-weight: 300;
    line-height: 1.25rem;
    overflow: hidden;
    /* width: 25vw; */
  }
`;

const posts = [
  {
    user: "1",
    summery:
      "hgdsghdsbdbncbshjdhsdhgdshgdsghdsbdbncbshjdhgdsghdsbdbncbshjdhsdhgdshgdsghdsbdbncbshjdhgdsghdsbdbncbshjdhsdhgdshgdsghdsbdbncbshjd"
  },
  {
    user: "2",
    summery: "sdysbdsjhdshhjsd"
  },
  {
    user: "3",
    summery: "sdysbdsjhdshhjsd"
  },
  {
    user: "4",
    summery: "sdysbdsjhdshhjsd"
  }
];

const Reviewpage = props => {
  return (
    <PaperContainer>
    <StyledPage>
      {posts.map(e => (
        <Card className="alignment">
          <CardContent className=" review-card ">
            {" "}
            <p> {e.summery}</p>
          </CardContent>
        </Card>
      ))}
      <Review></Review>
    </StyledPage>
    </PaperContainer>
  );
};

export default Reviewpage;
