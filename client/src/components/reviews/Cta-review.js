import React, { useState } from "react";
import { Button, Modal } from "@material-ui/core";
import CreateIcon from "@material-ui/icons/Create";
import { IconReview, StyledReview } from "./styledReview";

const Review = props => {
  const [open, setOpen] = useState(false);

  const [post, setPost] = useState();

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = e => {
    let value = e.target.value;
    setPost(value);
    console.log(e);
  };

  const postHandler = e => {
    e.preventDefault();

    // setPost(value);
    console.log(post);
    // let postData = post
  };
  const body = (
    <React.Fragment>
      <StyledReview>
        <h1> VOICE YOUR OPINION! AASYA FOUNDATION HEARS YOU </h1>
        <div className="review_form">
          <div>
            <label style={{ paddingTop: "20px" }}> Your review </label>
          </div>
          <form onSubmit={postHandler}>
            <div className="textContainer">
              <textarea
                className="review text-content"
                placeholder="share your opinion"
                maxLength="4000"
                column="1000"
                onChange={handleChange}
              />

              <Button
                color="primary"
                variant="contained"
                style={{ width: "100%" }}
                onClick={postHandler}
              >
                POST YOUR REVIEW{" "}
              </Button>
            </div>
          </form>
        </div>
      </StyledReview>
    </React.Fragment>
  );
  return (
    <div>
      <IconReview>
        {" "}
        <CreateIcon type="button" onClick={handleOpen} />{" "}
      </IconReview>

      <Modal open={open} onClose={handleClose}>
        {body}
      </Modal>
    </div>
  );
};

export default Review;
