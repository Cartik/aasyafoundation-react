import styled from "styled-components";

export const IconReview = styled("div")`
  align-items: center;
  border-radius: 500px;
  bottom: 16px;
  cursor: pointer;
  display: flex;
  height: 5vh;
  justify-content: center;
  min-width: 32px;
  position: fixed;
  right: 3rem;
  width: fit-content;
`;

export const StyledReview = styled("div")`
  height: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  margin: 0px 250px;

  h1 {
    font-size: 1.5rem;
    padding: 2rem 3rem;
    margin: 1rem 5rem;
  }

  .review_form {
    background: #f2f2f2;
    border-radius: 5px;
    margin-bottom: 30px;
    position: relative;
    color: #7e7e7e;
    height: 75vh;
    padding: 30px 40px;
    margin: 40px 30px;
  }
  label {
    font-size: 1.2rem;
    font-weight: 700;
    padding: 0 0 5px;
    display: block;
    margin: 0;
    text-align: left;
    white-space: normal;
    color: #7e7e7e;
  }

  .textContainer {
    background: none transparent;
    width: 100%;
    white-space: pre-wrap;
    box-sizing: border-box;
    margin: 0;
    line-height: 1.5;
    padding: 8px 6px;
    resize: vertical;
    letter-spacing: 0.5px;
  }
  .text-backdrop {
    position: absolute;
    top: 0;
    right: -99px;
    bottom: 0;
    left: 0;
    padding-right: 99px;
    overflow-x: hidden;
    overflow-y: auto;
  }
  .text-content {
    background: none transparent;
    width: 100%;
    white-space: pre-wrap;
    box-sizing: border-box;
    margin: 0;
    line-height: 1.5;
    padding: 8px 6px;
    resize: vertical;
    letter-spacing: 0.5px;
  }
  .text-highlight {
    width: auto;
    height: auto;
    border-color: transparent;
    white-space: pre-wrap;
    word-wrap: break-word;
    color: transparent;
    overflow: hidden;
  }
  .review {
    height: 200px;
    width: 100%;
    border: 1px solid #a9a9a9;
    margin: 0 0 25px;
    color: #292929;
    padding: 8px 6px;
  }
`;
