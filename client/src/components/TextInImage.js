import { Typography } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import React from "react";
const styles = (theme) => ({
  imageTextContainer: {
    position: "relative",
    textAlign: "center",
    color: "#FFF",
    display:'flex'
  },
  imageTextItem: {
    textAlign: "center",
    color: "#FFF",
  },
  space:{
    paddingLeft:'4px',
    paddingRight:'4px'
  }
});
class TextInImage extends React.Component {
  render() {
    const { classes, variant, text } = this.props;
    return (
        <div className={classes.imageTextContainer}>
             <Typography variant={variant}>{text}</Typography>
        </div>
    );
  }
}

export default withStyles(styles)(TextInImage);
