import React from "react";
import {
  Typography,
  GridListTile,
  GridListTileBar,
  IconButton,
  Grid,
  Card,
  CardContent,
  CardActions,
  Button,
  Divider,
  Avatar
} from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import InfoIcon from "@material-ui/icons/Info";
import CallInfo from "@material-ui/icons/Call";
import MailInfo from "@material-ui/icons/Mail";
import BackArrow from "@material-ui/icons/ArrowBack";
import ReactCardFlip from "react-card-flip";
import grey from "@material-ui/core/colors/grey"
import Fade from 'react-reveal/Fade';
const styles = theme => ({
  gridContainer: {
    padding: 15
  },
  gridList: {
    transform: "translateZ(0)",
    alignItems: "stretch"
  },
  gridListItem: {
    '& li': {
      listStyle: "none",
      height: 250
    },
    '& li > div': {
      borderRadius: 8
    }
  },
  cardRoot: {
    textAlign:'center',
    boxShadow: "0 8px 40px -12px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px -12.125px rgba(0,0,0,0.3)"
    },

  },
  avatar:{
    margin:'auto',
    height:70,
    width:70
  },
  heading: {
    fontSize: 18,
    fontWeight: 'bold',
    letterSpacing: '0.5px',
    marginTop: 8,
    marginBottom: 4,
  },
  subheader: {
    fontSize: 14,
    color: grey[500],
    marginBottom:8
  },
  action:{
    display:'flex',
    justifyContent: 'space-around',
  },
  backButtonDiv:{
    display:'flex',
  }
});

const cityList = [
  {
    id: 1,
    title: "Hyderabad",
    image:
      "https://cdn.pixabay.com/photo/2015/08/19/15/41/charminar-896162_1280.jpg",
    cityName: "Hyderabad"
  },
  {
    id: 2,
    title: "Bangalore",
    image:
      "https://cdn.pixabay.com/photo/2015/04/23/02/24/mysore-palace-at-monstre-735599_1280.jpg",
    cityName: "Bangalore"
  },
  {
    id: 3,
    title: "Chennai",
    image:
      "https://cdn.pixabay.com/photo/2018/05/16/10/44/chennai-3405413_1280.jpg",
    cityName: "Chennai"
  },
  {
    id: 4,
    title: "Coimbatore",
    image:
      "https://cdn.pixabay.com/photo/2019/03/02/09/08/shiva-4029469_1280.jpg",
    cityName: "Coimbatore"
  },
  {
    id: 5,
    title: "Vizag",
    image:
      "https://cdn.pixabay.com/photo/2019/12/12/08/36/vizag-4690108_1280.jpg",
    cityName: "Vizag"
  },
  {
    id: 6,
    title: "Vijayawada",
    image:
      "https://www.oyorooms.com/travel-guide/wp-content/uploads/2019/03/Kanaka-Durga-Temple.jpg",
    cityName: "Vijayawada"
  },
  {
    id: 7,
    title: "Jangoan",
    image:
      "https://en.wikipedia.org/wiki/Kakatiya_Kala_Thoranam#/media/File:Warangal_fort.jpg",
    cityName: "Jangoan"
  },
  {
    id: 8,
    title: "Guntur",
    image:
      "https://cdn.pixabay.com/photo/2017/09/23/18/54/cayenne-peppers-2779828_1280.jpg",
    cityName: "Guntur"
  },
  {
    id: 9,
    title: "Nellore",
    image:
      "https://cdn.pixabay.com/photo/2017/09/23/18/54/cayenne-peppers-2779828_1280.jpg",
    cityName: "Nellore"
  }
];

class AasyaCities extends React.Component {
  state ={

  };
  handleInfoClick = cityId => {
    this.setState({[cityId] : true});
  };
  handleBackButtonClick = cityId =>{
    this.setState({[cityId] : false})
  }
  render() {
    const { classes} = this.props;
    return (
      <div>
        <Typography align="center" variant="h4">
          Where we are{" "}
        </Typography>
        <Grid container spacing={2} className={classes.gridContainer}>
          {cityList.map(city => (
            <Grid item xs={12} sm={4} lg={3} xl={2} className={classes.gridListItem}>
              <ReactCardFlip isFlipped={this.state[city.id] === undefined ? false : this.state[city.id]}>
                <Fade bottom>
                <GridListTile key={city.id} >
                  <img src={city.image} alt={city.cityName} />
                  <GridListTileBar
                    title={city.cityName}
                    titlePosition="bottom"
                    actionIcon={
                      <IconButton
                        color="primary"
                        onClick={() => this.handleInfoClick(city.id)}
                      >
                        <InfoIcon />
                      </IconButton>
                    }
                  ></GridListTileBar>
                </GridListTile>
                </Fade>
                <Card className={classes.cardRoot}>
                  <CardContent className={classes.cardContent}>
                    <div className ={classes.backButtonDiv}>
                      <IconButton onClick = { () => this.handleBackButtonClick(city.id)}>
                        <BackArrow/>
                      </IconButton>
                    </div>
                    <Avatar className ={classes.avatar}src ={city.image}/>
                    <Typography className={classes.heading}variant="h3">
                      Karthik Uppala 
                    </Typography>
                    <Typography  className={classes.subheader} component="span">
                      {city.cityName}
                    </Typography>
                    </CardContent>
                    <Divider variant="middle"/>
                    <CardActions className={classes.action}>
                      <Button startIcon ={<CallInfo />}href="tel:9666616006" size="small" variant="contained">Call</Button>
                      <Divider orientation="vertical" />
                      <Button startIcon ={<MailInfo />}href="tel:9666616006" size="small" variant="contained">Mail</Button>
                    </CardActions>
                </Card>
              </ReactCardFlip>
            </Grid>
          ))}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(AasyaCities);
