import React from "react";
import {
  Card,
  CardHeader,
  CardContent,
  Typography,
  Grid,
  Paper,
  TextField,
  Button,
  Icon
} from "@material-ui/core";
import LocationIcon from "@material-ui/icons/LocationOn";
import CallIcon from "@material-ui/icons/Call";
import MailIcon from "@material-ui/icons/Mail";
import { withStyles } from "@material-ui/styles";
import grey from "@material-ui/core/colors/grey"
const styles = theme => ({
  root: {
    overflow: "initial",
    position: "relative",
  },
  gridContainer: {
    paddingTop: 50,
    paddingLeft: 16,
    paddingRight: 16
  },
  icon: {
    fontSize: 30,
    marginRight: 10
  },
  addressDiv: {
    paddingBottom: 10,
    display: "flex"
  },
  buttonContainer: {
    textAlign: "center",
    paddingTop: 15
  },
  marginAuto: {
    margin: "auto"
  },
  subdata: {
    fontSize: 14,
    color: grey[500],
  },
  subheader :{
    fontSize: 14,
    color: grey[700],
  }
});
class Feedback extends React.Component {
  render() {
    const { classes } = this.props;
    return (
      <Paper className={classes.root} elevation={2}>
        <Grid
          container
          direction="row"
          spacing={4}
          className={classes.gridContainer}
        >
          <Grid item xs={12} sm={6} lg={6} className={classes.marginAuto}>
            <Typography gutterBottom variant="h4">
              Send us a message
            </Typography>
            <Typography className={classes.subheader} component="span">
              Contact anything related to aasyafoundation we will get back to
              you as soon as possible
            </Typography>
            <TextField
              label="Name"
              margin="normal"
              autoFocus
              fullWidth
              size="small"
              className={classes.textField}
            />
            <TextField
              label="Email address"
              margin="normal"
              fullWidth
              size="small"
            />
            <TextField label="Phone" margin="normal" fullWidth size="small" />
            <TextField
              label="Your Message"
              margin="normal"
              multiline={true}
              rows={5}
              fullWidth
            />
            <div className={classes.buttonContainer}>
              <Button color="primary" variant="contained">
                Contact Us
              </Button>
            </div>
          </Grid>
          <Grid item xs={12} sm={5} lg={5} className={classes.marginAuto}>
            <Grid
              container
              direction="column"
              justify="center"
              alignItems="center"
            >
              <Grid item xs={12}>
                <div className={classes.addressDiv}>
                  <LocationIcon
                    color="primary"
                    className={classes.icon}
                  ></LocationIcon>

                  <div>
                    <Typography gutterBottom variant="h6">
                      Find Us At
                    </Typography>
                    <p className ={classes.subdata}>
                      Aasya Foundation Plot no. 151,
                      <br />
                      Rd Number 5, R K H Colony,
                      <br />
                      A S Rao Nagar Secunderabad, <br />
                      Telangana 500062
                    </p>
                  </div>
                </div>
                <div className={classes.addressDiv}>
                  <CallIcon color="primary" className={classes.icon}></CallIcon>
                  <div>
                    <Typography gutterBottom variant="h6">
                      Call Us
                    </Typography>
                    <p className ={classes.subdata}>+91.9492913475</p>
                  </div>
                </div>
                <div className={classes.addressDiv}>
                  <MailIcon color="primary" className={classes.icon}></MailIcon>
                  <div>
                    <Typography gutterBottom variant="h6">
                      Mail Us
                    </Typography>
                    <p className ={classes.subdata}>
                      aasyafoundation@gmail.com
                      <br />
                    </p>
                  </div>
                </div>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default withStyles(styles)(Feedback);
