import React from 'react'
import { Map , GoogleApiWrapper , Marker} from 'google-maps-react'

const mapStyles = {
    width: '100%',
    height: '100%',
    position:'relative'
  };
class ContactUsMap extends React.Component{
    render(){
        return(
            <Map google={this.props.google}
            zoom={15}
            style={mapStyles}
            disableDefaultUI= {true}
            initialCenter={{ lat: 17.477686, lng: 78.548945}}>
                <Marker position={{ lat: 17.477686, lng: 78.548945}}/>
            </Map>
        );
    }
}

export default GoogleApiWrapper({
    apiKey: 'AIzaSyACsX3geK4GnRSlPugvgGt2-7pwwS52Zro'
})(ContactUsMap)