import React from 'react'
import { Paper, Typography, Divider, Card, CardHeader, CardContent, TableBody, TableCell, TableRow } from '@material-ui/core'
import {withStyles} from '@material-ui/styles'

const styles = theme =>({
    card: {
        marginTop:20,
        borderRadius: theme.spacing(0.5),
        transition: '0.3s',
        overflow: 'initial',
        background: '#ffffff',
        display:'flex',
        flexDirection:'column'
      },
      header:{
          height:50,
          marginTop:-25,
          backgroundColor: '#3f51b5',
          borderRadius:theme.spacing(8),
          marginLeft:'auto',
          marginRight:'auto',
          color:'white !important'
      },
      content:{
          textAlign:'left',
          overflow:'auto'
      }
})

const rows = [
    {
        id:1,
        name:'Sample 1',
        amount:2500,
        cause:'Education'
    },
    {
        id:2,
        name:'Sample 2',
        amount:25000,
        cause:'Surgery'
    },
    {
        id:3,
        name:'Sample 3',
        amount:2500,
        cause:'Education'
    },
    {
        id:1,
        name:'Sample 1',
        amount:2500,
        cause:'Education'
    },
    {
        id:2,
        name:'Sample 2',
        amount:25000,
        cause:'Surgery'
    },
    {
        id:3,
        name:'Sample 3',
        amount:2500,
        cause:'Education'
    }
]
class Beneficiaries extends React.Component{
    render(){
        const {classes} = this.props;
        return(
            <Card className={classes.card}>
                    <CardHeader className={classes.header} title= {<Typography variant="h6">Our Beneficiaries</Typography>}/>
                    <CardContent>
                        <TableBody>
                            {
                                rows.map(row => (
                                    <TableRow>
                                        <TableCell>{row.name}</TableCell>
                                        <TableCell>{row.amount}</TableCell>
                                        <TableCell>{row.cause}</TableCell>
                                    </TableRow>
                                ))
                            }
                        </TableBody>
                    </CardContent>
            </Card>
        )
    }
}

export default withStyles(styles)(Beneficiaries)