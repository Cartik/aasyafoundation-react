import React from "react";
import { CardMedia, Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles"
import classNames from "classnames"
import Fade from 'react-reveal/Fade';

const styles = theme => ({
    cardImage: {
        height: '30rem',
        boxShadow: "0 6px 5px 0px rgba(0, 0, 0, 0.2)",
        backgroundPosition:'center',
        backgroundSize:'cover',
        border:'#fff 2px solid',
        [theme.breakpoints.down('sm')]: {
          height: '15rem',
        },
      },
    padding:{
        padding:'0px 16px'
    },
    gridItem:{
        zIndex:1
    },
    margin1:{
        marginTop:'-80px'
    },
    margin2:{
        marginTop:'-40px'
    },
    margin3:{
        marginTop:'-120px'
    },
    margin4:{
        marginTop:'-60px'
    }
})

class ZigZagImages extends React.Component {
  render() {
      const { classes ,images} = this.props;
    return (
    <div className ={classes.padding}>
      {
        images.length > 3 && (
          <Grid container wrap="noWrap" spacing={2}>
            
          <Grid item xs ={3} className={classNames(classes.gridItem,classes.margin1)}>
          <Fade top delay={1000}>
            <CardMedia className ={classes.cardImage}image={images[0].largeImageURL}></CardMedia>
            </Fade>
          </Grid>
          <Grid item xs ={3} className={classNames(classes.gridItem,classes.margin2)}>
          <Fade bottom delay={1000}>
            <CardMedia className ={classes.cardImage} image={images[1].largeImageURL}></CardMedia>
            </Fade>
          </Grid>
          <Grid item xs ={3} className={classNames(classes.gridItem,classes.margin3)}>
          <Fade top delay={1000}> 
            <CardMedia className ={classes.cardImage} image={images[2].largeImageURL}></CardMedia>
          </Fade>
          </Grid>
          <Grid item xs ={3} className={classNames(classes.gridItem,classes.margin4)}> 
          <Fade bottom delay={1000}>

            <CardMedia className ={classes.cardImage} image={images[3].largeImageURL}></CardMedia>
            </Fade>
          </Grid>
        </Grid>
        )
      
      }
      
    </div>
    );
  }
}

export default withStyles(styles)(ZigZagImages);
