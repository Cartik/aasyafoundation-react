import React from "react";
import {
  Grid,
  Container,
  TextField,
  Switch,
  Typography,
  Slide,
  Button,
  InputLabel,
} from "@material-ui/core";
import { DropzoneArea } from "material-ui-dropzone";
import { withStyles } from "@material-ui/styles";
import { storage } from "../config/firebase-config";
import AasyaApi from "../api/AasyaApi";
import SnackBar from "../components/SnackBar";
import { ValidatorForm, TextValidator } from "react-material-ui-form-validator";

const styles = (theme) => ({
  root: {
    paddingTop: theme.spacing(2),
  },
});

const initailState = {
  story: {
    hasYouTubeVideo: false,
    files: [],
    imageName: "",
    imageUrl: "",
    name: "",
    storyDescription: "",
    youtubeVideoLink: "",
  },
  uploadBoolean: false,
  progress: 0,
  isSubmitted: false,
  snackbar: {
    message: "",
    variant: "",
    open: false,
  },
};
class SuccessStoryAdd extends React.Component {
  state = { ...initailState };

  handleSwitch = (event) => {
    if (event.target.checked) {
      this.setState({ story: { ...this.state.story, hasYouTubeVideo: true } });
    } else {
      this.setState({ story: { ...this.state.story, hasYouTubeVideo: false } });
    }
  };

  handleSnackBarClose = () => {
    this.setState({snackbar:{...this.state.snackbar,open:false}})
  }
  handleFileOnChange = (files) => {
    const snackbar = {};
            snackbar.message = "File Uploaded Successfully";
            snackbar.variant = "success";
            snackbar.open = true;
    this.setState({ story:{...this.state.story,files},snackbar});
  };
  handleSubmitStory = () => {
    this.setState({ isSubmitted: true });
    if (true) {
      // TODO Validations need to be performed
      AasyaApi.post("/stories/addSuccessStory", this.state.story).then(
        (response) => {
          if (response.status === 200) {
            const snackbar = {};
            snackbar.message = response.data.message;
            snackbar.open = true;
            snackbar.variant = "success";
            this.setState({ ...initailState, snackbar });
          }
        },
        (error) => {
          const snackbar = {};
          snackbar.message = error.message;
          snackbar.open = true;
          snackbar.variant = "error";
          this.setState({ snackbar });
        }
      );
    }
  };
  handleFileUpload = (files) => {
    this.setState({ uploadBoolean: true });
    let imageName = "test-upload" + Date.now();
    let uploadImage = storage
      .ref(`images/${imageName}`)
      .put(this.state.story.files[0]);
    uploadImage.on(
      "state_changed",
      (snapshot) => {
        this.setState({
          progress: Math.round(
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100
          ),
        });
      },
      (error) => {
        alert(error);
      },
      () => {
        storage
          .ref("images")
          .child(imageName)
          .getDownloadURL()
          .then((url) => {
            const snackbar = {};
            snackbar.message = "File Uploaded Successfully";
            snackbar.open = true;
            snackbar.variant = "success";
            this.setState({
              story: { ...this.state.story, imageUrl: url, imageName },
              uploadBoolean: false,
              snackbar,
            });
          });
      }
    );
  };
  render() {
    const { classes } = this.props;
    return (
      <Container>
        <ValidatorForm ref="form" onSubmit={this.handleSubmitStory}>
          <Grid container spacing={2} className={classes.root}>
            <Grid item xs={12} md={6}>
              <TextValidator
                size="small"
                variant="outlined"
                name="name"
                fullWidth
                label="Name of Beneficiary"
                value={this.state.story.name}
                onChange={(event) =>
                  this.setState({
                    story: { ...this.state.story, name: event.target.value },
                  })
                }
                validators={["required"]}
                errorMessages={["Please Enter the name"]}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <TextValidator
                size="small"
                variant="outlined"
                name="storyDescription"
                fullWidth
                multiline
                rows={4}
                label="Description"
                value={this.state.story.storyDescription}
                onChange={(event) =>
                  this.setState({
                    story: {
                      ...this.state.story,
                      storyDescription: event.target.value,
                    },
                  })
                }
                validators={["required"]}
                errorMessages={["Please Enter the Story content"]}
              />
            </Grid>
            <Grid item xs={12}>
              <Grid container spacing={1}>
                <Grid item>
                  <Typography variant="h6">
                    Is success story video uploaded to youtube ?
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="body1">Yes</Typography>
                </Grid>
                <Grid item>
                  <Switch
                    checked={this.state.story.hasYouTubeVideo}
                    size="small"
                    onChange={this.handleSwitch}
                  ></Switch>
                </Grid>
                <Grid item>
                  <Typography variant="body1"> No</Typography>
                </Grid>
              </Grid>
            </Grid>
            {this.state.story.hasYouTubeVideo && (
              <Slide direction="left" in={this.state.story.hasYouTubeVideo}>
                <Grid item xs={12} sm={12} md={12}>
                  <TextValidator
                    size="small"
                    variant="outlined"
                    name="youtubeVideoLink"
                    fullWidth
                    label="Youtube Video Link"
                    value={this.state.youtubeVideoLink}
                    onChange={(event) =>
                      this.setState({
                        story: {
                          ...this.state.story,
                          youtubeVideoLink: event.target.value,
                        },
                      })
                    }
                    validators={
                      this.state.hasYouTubeVideo == true ? ["required"] : ""
                    }
                    errorMessages={
                      this.state.hasYouTubeVideo == true
                        ? ["Please Enter the Story content"]
                        : ""
                    }
                  />
                </Grid>
              </Slide>
            )}
            <Grid item xs={12} md={6}>
              <DropzoneArea
                acceptedFiles={["image/jpeg", "image/png", "image/bmp"]}
                filesLimit={1}
                onChange={this.handleFileOnChange}
                showAlerts={false}
              ></DropzoneArea>
              {this.state.isSubmitted && this.state.story.imageUrl ==='' && (
                <InputLabel  error={true}>
                  Please Upload the Image Before 
                </InputLabel>
              )}
            </Grid>
            <Grid item xs={12} md={6}>
              <Button
                variant="outlined"
                color="secondary"
                size="small"
                onClick={this.handleFileUpload}
              >
                Upload
              </Button>
              {this.state.uploadBoolean && (
                <Typography variant="h6">{`Upload is ${this.state.progress} % done`}</Typography>
              )}
            </Grid>
            <Grid item xs={12} md={6}>
              <Button type="submit" variant="contained" color="primary">
                Submit Story
              </Button>
            </Grid>
          </Grid>
        </ValidatorForm>
        <SnackBar
          variant={this.state.snackbar.variant}
          open={this.state.snackbar.open}
          message={this.state.snackbar.message}
          onClose = {this.handleSnackBarClose}
        ></SnackBar>
      </Container>
    );
  }
}

export default withStyles(styles)(SuccessStoryAdd);
