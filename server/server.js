const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const routes = require("./routes/SuceesStories.routes");
const cors = require('cors')
const app = express();


require("dotenv").config();

const port = process.env.PORT || 3100;
app.get("/", (req, res) => res.send("Hello World!"));

mongoose
  .connect(process.env.DB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log(`Database connected successfully`))
  .catch((err) => console.log(err));

app.use(cors());
app.use(bodyParser.json());

app.use("/stories", routes);
app.listen(port, () =>
  console.log(`Example app listening at http://localhost:${port}`)
);
