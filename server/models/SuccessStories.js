const mangoose = require('mongoose')
const Schema = mangoose.Schema;

const SuccessStoriesSchema = new Schema({
    name:{
        type:String,
        required:[true,'Name is required']
    },
    storyDescription:{
        type:String,
        required:[true,'Success Story Content Need to be specified']
    },
    hasYouTubeVideo:{
        type:Boolean,
        required:[true,'Please Specify If youtube video uploaded']
    },
    youtubeUrl:{
        type:String
    },
    imageName:{
        type:String,
        required:[true,'Please specify uploaded image name']
    },
    imageUrl:{
        type:String,
        required:[true,'Please specify uploaded image url']
    }
})

module.exports = mangoose.model('SuccessStories', SuccessStoriesSchema)