const express = require('express')
const router = express.Router();
const SuccessStories = require('../models/SuccessStories')

router.get('/getSuccessStories',(req,res,next) =>{
    SuccessStories.find().then(data => res.json(data))
    .catch(next)
})

router.post('/addSuccessStory',(req,res,next)=>{
    SuccessStories.create(req.body).then(story=>res.status(200).json({'message':'Success Story added Successfully'}))
    .catch(err => {
        console.log(err)
        res.status(422).json(`error:${err.message}`);
    })
})


module.exports = router;